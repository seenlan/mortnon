package fun.mortnon.mortnon.service.base;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 公共服务接口
 *
 * @author dongfangzan
 * @date 20.4.21 2:37 下午
 */
public interface BaseService<T> extends IService<T> {
}
