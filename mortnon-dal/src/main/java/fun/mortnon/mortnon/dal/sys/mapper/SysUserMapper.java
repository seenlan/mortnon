package fun.mortnon.mortnon.dal.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.mortnon.mortnon.dal.sys.entity.SysUser;
import org.springframework.stereotype.Repository;

/**
 * @author dongfangzan
 * @date 28.4.21 3:41 下午
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {
}
